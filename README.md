# README #

This repo is for data storage and ASCENDS model training. To use any data or models contained herein requires
the user to have previously installed ASCENDS (https://github.com/ornlpmcp/ASCENDS) on their machine. All code
is run from wherever that ASCENDS directory is. Then, you direct the ASCENDS scripts to use data from the
"data" folder and store results in the "output" folder and associated sub-directories. 

### What is this repository for? ###

* This repo is only for data storage and model storage from the ASCENDS python code (https://github.com/ornlpmcp/ASCENDS)
* Version 0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* See the ASCENDS README.md to learn how to setup and run ASCENDS (https://github.com/ornlpmcp/ASCENDS)

### Contribution guidelines ###

* There are no tests

### Who do I talk to? ###

* ladshawap@ornl.gov